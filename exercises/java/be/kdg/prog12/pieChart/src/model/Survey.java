package model;

import java.util.HashMap;
import java.util.Map;

public class Survey {
    private final Map<String, Integer> data = new HashMap<>();

    public Survey() {
    }
    public void setEntry(String answer, int percentage) {
        data.put(answer, percentage);
    }

    public Map<String, Integer> getData() {
        return data;
    }
}
