package view;

import javafx.geometry.Insets;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class PieView extends BorderPane {

    private PieChart pie;
    private ComboBox<String> comboBox;
    private TextField textField;
    private Label label;

    public PieView() {
        this.initialiseNodes();
        this.layoutNodes();
    }

    private void initialiseNodes() {
        this.pie = new PieChart();
        this.comboBox = new ComboBox<>();
//        comboBox.getItems().addAll("HBox", "VBox", "BorderPane", "GridPane");
//        comboBox.setPromptText("What is your favourite layout ");

        comboBox.setEditable(true);
        this.textField = new TextField();
        textField.setText("Percentage");
        this.label = new Label();
    }

    private void layoutNodes() {
        pie.setTitle("Where do I type semicolons?");
        this.setCenter(pie);
        this.setBottom(addBox());
    }

    private HBox addBox() {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(10));
        label.setText("Change a slice");
        hbox.setSpacing(10);
        hbox.getChildren().addAll(label, comboBox, textField);
        return hbox;
    }

    PieChart getPie() {
        return pie;
    }

    ComboBox<String> getComboBox() {
        return comboBox;
    }

    TextField getTextField() {
        return textField;
    }

}
