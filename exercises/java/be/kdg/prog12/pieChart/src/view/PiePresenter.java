package view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import model.Survey;

import java.util.Collections;
import java.util.Map;

public class PiePresenter {
    private final Survey model;
    private final PieView view;

    public PiePresenter(Survey model, PieView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        view.getTextField().setOnAction(e -> setValuesToPie());
        updateView();
    }

    private void updateView() {
        view.getComboBox().getItems().clear();
        view.getComboBox().getItems().addAll(model.getData().keySet());
        ObservableList<PieChart.Data> data = FXCollections.observableArrayList();
        for (Map.Entry<String, Integer> entry : model.getData().entrySet()) {
            data.add(new PieChart.Data(entry.getKey(), entry.getValue()));
            System.out.println(data.toString());

        }
        view.getPie().setData(data);
    }

    public void setValuesToPie() {
        try {
            int percentage = Integer.parseInt(view.getTextField().getText().trim());
            String answer = view.getComboBox().getValue();
            model.setEntry(answer, percentage);
            updateView();
        } catch (NumberFormatException e) {
            view.getTextField().setText("Error Type in a Number ");
        }

    }

//    public void addWindowEventHandlers() {
//        view.getScene().getWindow();
//    }
}
