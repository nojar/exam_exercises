import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Survey;
import view.PiePresenter;
import view.PieView;

public class PieRunner extends Application {

    @Override
    public void start(Stage stage) {
        Survey model = new Survey();
        PieView view = new PieView();
        new PiePresenter(model, view);
        stage.setScene(new Scene(view));
        stage.setTitle("Pie Chart");
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
    

}
