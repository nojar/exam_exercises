public class BankAccountValidator {
    private static final int LENGTH = 12;

    public static void validateAccount(String account) throws BankAccountException {
        //check if account length != length (12)
        if (account.length() != LENGTH) {
            throw new BankAccountException("Number must have 12 digits");
            //check if account number only has strings
        } else if (!account.matches("[0-9]+")) {
            throw new BankAccountException("");
            //call isValidNumber functionString must be numeric
        } else if (!isValidNumber(account)) {
            throw new BankAccountException("Wrong account number");
        }
    }

    private static boolean isValidNumber(String account) {
        long accountNumber;
        long accountNumberRemainder;
        int controlNumber;

        //This is the first ten digits(substring 0 to 10)
        accountNumber = Long.parseLong(account.substring(0, 10));
        accountNumberRemainder = accountNumber % 97;

        controlNumber = Integer.parseInt(account.substring(10, 12));
        //if remainder == 0 then controlNumber == 97
        if (accountNumberRemainder == 0) {
            return controlNumber == 97;
        } else {
            return accountNumberRemainder == controlNumber;
        }
    }
}
