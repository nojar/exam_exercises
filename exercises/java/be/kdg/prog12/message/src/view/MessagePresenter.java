package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
;import java.io.*;
import java.util.Base64;

public class MessagePresenter {
    private final MessageView view;

    public MessagePresenter(MessageView view) {
        this.view = view;

        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        final EventHandler<ActionEvent> eventHandler = event -> updateView();
        view.getForegroundPicker().setOnAction(eventHandler);
        view.getBackgroundPicker().setOnAction(eventHandler);

        view.getMessageField().textProperty().addListener((observable, oldValue, newValue) -> updateView());

        view.getLoadMenuItem().setOnAction(event -> {
            try {
//                File file = new File("ohemgee.txt");
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Open");
                File file = fileChooser.showOpenDialog(view.getScene().getWindow());
                if (file != null) {

                    FileInputStream fis = new FileInputStream(file);
                    DataInputStream dis = new DataInputStream(fis);
                    //Foreground
                    short FGR = dis.readShort();
                    short FGG = dis.readShort();
                    short FGB = dis.readShort();

                    //Background
                    short BGr = dis.readShort();
                    short BGg = dis.readShort();
                    short BGb = dis.readShort();

                    byte[] buffer = new byte[50];

                    int numberOfBytes = dis.read(buffer);
                    String EncodedText
                            = new String(buffer, 0, numberOfBytes);
                    String DecodedText
                            = new String(Base64.getDecoder().decode(EncodedText));


                    view.getForegroundPicker().setValue(Color.rgb(FGR, FGG, FGB));
                    view.getBackgroundPicker().setValue(Color.rgb(BGr, BGg, BGb));
                    view.getMessageField().setText(DecodedText);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        view.getSaveMenuItem().setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("All files", "*.*"));
            fileChooser.setTitle("Save");
            File file = fileChooser.showSaveDialog(view.getScene().getWindow());
            try {
                if (file != null) {
                    FileOutputStream fos = new FileOutputStream(file);
                    DataOutputStream dos = new DataOutputStream(fos);
                    dos.writeBytes(view.getMessageField().getText());
                    String text = view.getMessageField().getText();

                    Color FG = view.getForegroundPicker().getValue();
                    Color BG = view.getBackgroundPicker().getValue();

                    //Foreground
                    short FGR = (short) (FG.getRed() * 255);
                    short FGG = (short) (FG.getGreen() * 255);
                    short FGB = (short) (FG.getBlue() * 255);
                    //Background
                    short BGr = (short) (BG.getRed() * 255);
                    short BGg = (short) (BG.getGreen() * 255);
                    short BGb = (short) (BG.getBlue() * 255);

                    //Foreground
                    dos.writeShort(FGR);
                    dos.writeShort(FGG);
                    dos.writeShort(FGB);

                    //Background
                    dos.writeShort(BGr);
                    dos.writeShort(BGg);
                    dos.writeShort(BGb);

                    dos.write(Base64.getEncoder().encode(text.getBytes()));
                    dos.close();
                    updateView();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }

    private void updateView() {
        final String message = view.getMessageField().getText();
        final Color foreground = view.getForegroundPicker().getValue();
        final Color background = view.getBackgroundPicker().getValue();
        view.showMessage(message, foreground, background);
    }
}
