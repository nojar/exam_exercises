import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.MessageModel;
import view.MessagePresenter;
import view.MessageView;

import java.io.FileNotFoundException;

public class MessageApplication extends Application {


    @Override
    public void start(Stage stage) throws FileNotFoundException {

        MessageModel model = new MessageModel();
        MessageView view = new MessageView();
        new MessagePresenter(view);
        stage.setScene(new Scene(view));
        stage.setTitle("Message");
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }


}
