package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

public class RetirementView extends BorderPane {
    TextField userInput;
    Button getInputButton;
    Label retirementYearLabel;
    Label messageLabel;
    Label titleLabel;
    Label birthYearLabel;
    Label clickMeLabel;

    public RetirementView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        // create and configure controls
        // button = new Button("...")
        // label = new Label("...")

        userInput = new TextField();
        userInput.setText("BirthYear");
        userInput.setPrefWidth(100);
        getInputButton = new Button("BirthYear > RetirementYear");
        retirementYearLabel = new Label();
        messageLabel = new Label();
        titleLabel = new Label("Enter Your BirthYear to Calculate\n        Your Retirement Year :)");
        titleLabel.setFont(Font.font("GEORGIA", FontWeight.BLACK, FontPosture.REGULAR, 20));
        birthYearLabel = new Label("BirthYear");
        clickMeLabel = new Label("Click Me");

    }

    private void layoutNodes() {
        setTop(titleLabel);
        setAlignment(titleLabel, Pos.CENTER);
        setMargin(userInput, new Insets(10));
        setLeft(userInput);
        setMargin(userInput, new Insets(10));
        setAlignment(userInput, Pos.CENTER);
        setCenter(getInputButton);
        setMargin(getInputButton, new Insets(10));
        setAlignment(getInputButton, Pos.CENTER);
        setRight(retirementYearLabel);
        setMargin(retirementYearLabel, new Insets(10));
        setAlignment(retirementYearLabel, Pos.CENTER);
    }

    // package-private getters
    TextField getUserInput() {
        return userInput;
    }

    Button getInputButton() {
        return getInputButton;
    }

    Label getRetirementYearLabel() {
        return retirementYearLabel;
    }
}
