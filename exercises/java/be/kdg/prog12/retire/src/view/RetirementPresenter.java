package view;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import model.Retirement;

public class RetirementPresenter {
    private final Retirement model;
    private final RetirementView view;

    public RetirementPresenter(
            Retirement model, RetirementView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void addEventHandlers() {
        // Adds event handlers (inner classes or lambdas)
        // to view controls
        // Event handlers: call model methods and
        // update the view.
//        view.getButton().setOnAction(e -> model.getRetirementYear());
        view.getInputButton().setOnAction( eventHandler());

    }

    EventHandler<ActionEvent> eventHandler() {
        return e -> {
            System.out.println("BIRTH YEAR: " + view.getUserInput().getText());
            updateView();
            view.getRetirementYearLabel().setText(String.valueOf(model.getRetirementYear(view.getUserInput(), view.getUserInput().getText())));
            System.out.println("You will retire at : " + (model.getRetirementYear(view.getUserInput(), view.getUserInput().getText())));
        };
    }

    private void updateView() {
        // fills the view with model data
    }
}
