import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Retirement;
import view.RetirementPresenter;
import view.RetirementView;

public class RetirementCalcRunner extends Application {


    @Override
    public void start(Stage stage) {

        Retirement model = new Retirement();
        RetirementView view = new RetirementView();
        new RetirementPresenter(model, view);
        stage.setScene(new Scene(view));
        stage.setTitle("Retirement Calculator");
        stage.setWidth(400.0);
        stage.setHeight(200.0);
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }


}
