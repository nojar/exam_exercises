package model;


import javafx.scene.control.TextField;

public class Retirement extends NumberFormatException{
    // private attributes
    private int birthYear;
    private final int RETIREMENT_AGE = 67;
    private int retirementYear;


    // Constructor
    public Retirement() {
    }

    //This method calculates retirement year
    public int getRetirementYear(TextField input, String userInput) {
        try {
            birthYear = Integer.parseInt(input.getText());
            retirementYear = getRETIREMENT_AGE() + birthYear;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return Integer.parseInt((String.valueOf(retirementYear)));
    }

    //Getters
    public int getBirthYear() {
        return birthYear;
    }

    public int getRETIREMENT_AGE() {
        return RETIREMENT_AGE;
    }
}