import java.util.TreeSet;

public class Driver implements Comparable<Driver> {
    private final String name;
    private final String nationality;

    TreeSet<Integer> yearsTheDriverWasChampion = new TreeSet<>();

    public Driver(String nationality, String name) {
        this.name = name;
        this.nationality = nationality;
    }

    //Getters
    public String getName() {
        return name;
    }

    public String getNationality() {
        return nationality;
    }

    public TreeSet<Integer> getYearsTheDriverWasChampion() {
        return yearsTheDriverWasChampion;
    }

    //Setter
    public void setYearsTheDriverWasChampion(Integer yearsTheDriverWasChampion) {
        this.yearsTheDriverWasChampion.add(yearsTheDriverWasChampion);
    }


    @Override
    public String toString() {
        return String.format("%s %s %s %s %s times champion %s\n", name, "(", nationality, ")", yearsTheDriverWasChampion.size(), yearsTheDriverWasChampion);

    }

    public int compareTo(Driver o) {
        int i = this.yearsTheDriverWasChampion.size() - o.yearsTheDriverWasChampion.size();
        return (i != 0) ? i : Integer.compare(o.yearsTheDriverWasChampion.last(), this.yearsTheDriverWasChampion.last());
    }

}

