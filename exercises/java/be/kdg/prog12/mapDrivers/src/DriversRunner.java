import java.util.*;

public class DriversRunner {
    private static final String[][] champions = {
            {"Germany", "Michael Schumacher"},
            {"Argentina", "Juan Manuel Fangio"},
            {"France", "Alain Prost"},
            {"German", "Sebastian Vettel"},
            {"Australia", "Jack Brabham"},
            {"United Kingdom", "Jackie Stewart"},
            {"Austria", "Niki Lauda"},
            {"Brasil", "Nelson Piquet"},
            {"Brasil", "Ayrton Senna"},
            {"United Kingdom", "Lewis Hamilton"}
    };

    private static final Integer[][] years = {
            {1994, 1995, 2000, 2001, 2002, 2003, 2004},
            {1951, 1954, 1955, 1956, 1957},
            {1985, 1986, 1989, 1993},
            {2010, 2011, 2012, 2013},
            {1959, 1960, 1966},
            {1969, 1971, 1973},
            {1975, 1977, 1984},
            {1981, 1983, 1987},
            {1988, 1990, 1991},
            {2008, 2014, 2015, 2017, 2018, 2019, 2020}
    };

    public static void main(String[] args) {
        Drivers drivers = new Drivers(champions, years);

        //This adds the "Jackie Stewart" to the driverNameList
        List<Driver> driverNameList = new ArrayList<>();
        driverNameList.add(drivers.getDriverByName("Jackie Stewart"));
        for (Driver d : driverNameList) {
            System.out.print(d);
        }


        List<Driver> champions = new ArrayList<>();
        champions.add(drivers.getDriverByYear(2000));
        champions.add(drivers.getDriverByYear(2005));
//        champions.add(drivers.getDriverByYear(1997));
//        champions.add(drivers.getDriverByYear(2001));
//        champions.add(drivers.getDriverByYear(2020));
//        champions.add(drivers.getDriverByYear(1960));
        for (Driver d : champions) {
            try {
                if (((drivers.getDriverByYear(2005)) != null)) {
                    System.out.printf("%d champion: %s%n", d.getYearsTheDriverWasChampion().ceiling(2005), d.getName());
                }
                if (((drivers.getDriverByYear(2000)) != null)) {
                    System.out.printf("%d champion: %s%n", d.getYearsTheDriverWasChampion().ceiling(2000), d.getName());

                } else if ((((drivers.getDriverByYear(2005) == null) || (drivers.getDriverByYear(2000)) == null))) {
                    System.out.printf("%d champion: %s%n", d.getYearsTheDriverWasChampion().ceiling(2005), "Not Listed");
                    System.out.printf("%d champion: %s%n", d.getYearsTheDriverWasChampion().ceiling(2000), "Not Listed");
                }
            } catch (NullPointerException e) {
                System.out.printf("%s%n", "Not Listed");
            }
        }

        //  This adds the drivers to a sortingList.
        List<Driver> sortingList = new ArrayList<>(drivers.getDrivers());
        Collections.sort(sortingList);//this print the sortingList in descending order
        Collections.reverse(sortingList);//this print the sortingList in ascending order
        System.out.println("DRIVERS BY TITLES");
        for (Driver d : sortingList) {
            System.out.print(d);
        }
    }
}