import java.util.*;

public class Drivers {

    //Attributes
    Map<String, Driver> driverByName = new HashMap<>();
    Map<Integer, Driver> driverByYear = new HashMap<>();
    Driver driver = new Driver("", "");

    //   Constructor without parameters
    public Drivers() {
    }

    //   Constructor with parameters
    public Drivers(Map<String, Driver> driverByName, Map<Integer, Driver> driverByYear) {
        this.driverByName = driverByName;
        this.driverByYear = driverByYear;
    }

    //    This constructor  accept the champions and years array as parameters
    public Drivers(String[][] champions, Integer[][] years) {
        for (int i = 0; i < champions.length; i++) {
            driver = new Driver(champions[i][0], champions[i][1]);
            for (Integer year : years[i]) {
                driver.setYearsTheDriverWasChampion(year);
                driverByYear.put(year, driver);
                driverByName.put(driver.getName(), driver);
            }
        }
    }

    //Getters
    public Driver getDriverByName(String name) {
        return driverByName.get(name);
    }

    public Driver getDriverByYear(Integer year) {
        return driverByYear.get(year);
    }

    List<Driver> getDrivers() {

        return new ArrayList<>(driverByName.values());
    }


}