import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.TileModel;
import view.TileView;
import view.Presenter;

public class TileRunner extends Application {


    @Override
    public void start(Stage primaryStage) {

        TileModel model = new TileModel();
        TileView view = new TileView();
        new Presenter(model, view);

        primaryStage.setTitle("Tiles");
        primaryStage.setScene(new Scene(view));
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

}
